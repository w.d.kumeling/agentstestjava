<#
.SYNOPSIS
    Build and run the software either such that all experiments are ran, or only specific datasets are run.
.DESCRIPTION
    This script builds the software, cd's into the directory it installs itself in, 
    generates the necessary datasets and runs them using the given settings.
    Without any parameters it simply runs all experiments as specified in the thesis.
    The software is installed in <directory of run.ps1>\app\build\install\app\
    That is also the directory that contains the generated datasets and the results of the tests.
.NOTES
    If $noInstallDistOrDirRemoval  is false, previous results are overwritten.
.PARAMETER nameOfDataset 
    The name of the dataset to run.
.PARAMETER sizesTo
    The sizes the dataset should be run on.
.PARAMETER runDataset
    Whether the dataset should also be run, or just generated.
.PARAMETER stratsToRun
    Override the strategies to run on the specified dataset(s).
.PARAMETER forcebhtreealso
    Whether to run every strategy also with the dialectical tree from Black and Hunter,
    as opposed to only doing that with the smart strategy.
.PARAMETER noInstallDistOrDirRemoval
    Whether to avoid running 'gradle installDist' and to avoid removing the install directory.
.EXAMPLE
    .\run.ps1
    Run all tests as specified in the thesis.
.EXAMPLE
    .\run.ps1 -nameOfDataset "teamdefeat" -sizesTo @(2,3)
    Run the team defeat dataset using n=2 and n=3 on all supported strategies
.EXAMPLE
    .\run.ps1 -nameOfDataset "teamdefeat" -sizesTo @(2,3) -runDataset $false
    Only generate the teamdefeat dataset for n=2 and n=3
#>
param(
    [String]$nameOfDataset = "",
    [uint32[]]$sizesTo = @(0),
    [bool]$runDataset = $true,
    [String[]]$stratsToRun = @("exhaust", "smart", "limitedcom", "limiteddiag"),
    [bool]$forcebhtreealso = $false,
    [bool]$noInstallDistOrDirRemoval = $false
)

#Requires -Version 6.0

$oldpwd = $PWD
$distdir = Join-Path $PWD -ChildPath "\app\build\install\app"

if ($isWindows) {
    $appbin = "app.bat"
}
else {
    $appbin = "app"
}

if ($isWindows) {
    $gradle = "gradlew.bat"
}
else {
    $gradle = "gradlew"
}

function runDataset {
    param (
        [String]$name,
        [int[]]$sizes
    )
    foreach ($size in $sizes) {
        $appbindir = Join-Path $PWD -ChildPath "\bin\$appbin"
        $datasetdir = Join-Path $distdir -ChildPath "\$name`_$size"

        foreach ($usetree in @($true, $false)) {
            foreach ($strat in $stratsToRun) {
                $Host.UI.RawUI.WindowTitle = "Running $name with $strat`_$usetree for size $size"

                if ( $usetree -and ($forcebhtreealso -or $strat -eq "smart" -or $strat -eq "exhaust") ) {
                    $cmdline = @("diag" , "-s", $strat, "-f", $strat, "--dottree", "--also-full-tree", "-l", "file" , "--usebhtree", "-vvvvvvv", "black_wi", $datasetdir)

                    & $appbindir $cmdline
                }
                elseif ($usetree -ne $true -and $strat -ne "exhaust" -and $strat -ne "smart") {
                    $cmdline = @("diag" , "-s", $strat, "-f", $strat, "--dottree", "--also-full-tree", "-l", "file", "-vvvvvvv", "black_wi", $datasetdir)
                
                    & $appbindir $cmdline
                }
    
            }
        }
    }
        
    
}

function genDataset {
    param (
        [String]$name,
        [int[]]$sizes
    )
    foreach ($size in $sizes) {
        $Host.UI.RawUI.WindowTitle = "Generating $name with size $size"

        $appbindir = Join-Path $PWD -ChildPath "\bin\$appbin"
        $datasetdir = Join-Path $distdir -ChildPath "\$name`_$size"
        & $appbindir generate $name $size $datasetdir
    }    
}

if($noInstallDistOrDirRemoval -eq $false) {
    Remove-Item -Recurse -Force $distdir
    $gradledir = Join-Path $oldpwd -ChildPath $gradle
    & $gradledir installDist
}

Set-Location "$distdir"

if ($nameOfDataset -ne "") {
    genDataset $nameOfDataset $sizesTo

    if ($runDataset -eq $true) {
        runDataset $nameOfDataset $sizesTo
    }

    Set-Location $oldpwd

    return
}
#else {

    # For floating conclusions
#    $sizes = @(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35)
#    genDataset "floating" $sizes
#    runDataset "floating" $sizes
    
    # For team defeat cascading
    #$sizes = @(2, 3)
    #genDataset "cascading" $sizes
    #runDataset "cascading" $sizes
    
    # For ambiguity
#    $sizes = @(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35)
#    genDataset "ambiguity" $sizes
#    runDataset "ambiguity" $sizes
#    
#    # For team defeat
#    $sizes = @(1, 2, 3, 4, 5, 6)
#    genDataset "teamdefeat" $sizes
#    runDataset "teamdefeat" $sizes
#}

Set-Location $oldpwd
