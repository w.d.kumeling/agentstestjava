<#
.SYNOPSIS
    Run the experiments described in the thesis. Caution: it takes a long while.
.DESCRIPTION
    This script runs the experiments as specified in the thesis. Afterwards, the app/build/install/app directory contains
    the results of the experiments in the form of folders named '<dataset>_<n>'. These folders must be copied into an empty directory. Then, using the readlogs command of this program, are the log_*.txt files converted into a single 'result.tsv' file.
#>
#Requires -Version 6.0
& .\run.ps1 -nameOfDataset ambiguity -sizesTo @(1..20) -stratsToRun exhaust
& .\run.ps1 -nameOfDataset teamdefeat -sizesTo @(1..4) -stratsToRun exhaust -noInstallDistOrDirRemoval $true
& .\run.ps1 -nameOfDataset floating -sizesTo @(1..35) -stratsToRun exhaust -noInstallDistOrDirRemoval $true

& .\run.ps1 -nameOfDataset ambiguity -sizesTo @(18..22) -stratsToRun smart -noInstallDistOrDirRemoval $true
& .\run.ps1 -nameOfDataset teamdefeat -sizesTo @(1..4) -stratsToRun smart -noInstallDistOrDirRemoval $true
& .\run.ps1 -nameOfDataset floating -sizesTo @(1..35) -stratsToRun smart -noInstallDistOrDirRemoval $true

& .\run.ps1 -nameOfDataset ambiguity -sizesTo @(1..20) -stratsToRun limiteddiag -noInstallDistOrDirRemoval $true
& .\run.ps1 -nameOfDataset teamdefeat -sizesTo @(1..4) -stratsToRun limiteddiag -noInstallDistOrDirRemoval $true
& .\run.ps1 -nameOfDataset floating -sizesTo @(1..35) -stratsToRun limiteddiag -noInstallDistOrDirRemoval $true

& .\run.ps1 -nameOfDataset ambiguity -sizesTo @(1..20) -stratsToRun limitedcom -noInstallDistOrDirRemoval $true
& .\run.ps1 -nameOfDataset teamdefeat -sizesTo @(1..4) -stratsToRun limitedcom -noInstallDistOrDirRemoval $true
& .\run.ps1 -nameOfDataset floating -sizesTo @(1..35) -stratsToRun limitedcom -noInstallDistOrDirRemoval $true
