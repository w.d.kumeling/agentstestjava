/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument;

import agentlib.AgentId;
import agentlib.Logger;
import agentstestjava.BeliefBaseParser;
import net.sf.tweety.arg.delp.DefeasibleLogicProgram;
import net.sf.tweety.arg.delp.parser.DelpParser;
import net.sf.tweety.arg.delp.syntax.DelpFact;
import net.sf.tweety.arg.delp.syntax.DelpRule;
import net.sf.tweety.commons.Parser;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DirectoryDelpParser extends BeliefBaseParser<DefeasibleLogicProgram, DelpLocution> {
    private Map<String, Integer> stringRuleToLevel;
    public Map<DelpRule, Integer> delpRuleToLevel;

    public String getPath() {
        return _path;
    }

    private String _path = "";

    public String levels = "";

    private AgentId id = new AgentId("PARSE");

    public DirectoryDelpParser(String path) throws IOException {
        super(new DelpParser(), path);

        _path = path;

        stringRuleToLevel = new HashMap<>();

        parseLevels(new File(path, "levels.txt"));
        delpRuleToLevel = getRuleMap();
    }

    public void parseLevels(String text) {
        levels = text;
        for (String s : text.split("\n")) {
            String[] fields = s.split(";");

            if(fields.length != 2) {
                throw new IllegalStateException("Syntax of file is wrong");
            }

            int level;

            level = Integer.parseInt(fields[1].trim());

            stringRuleToLevel.put(fields[0].trim(), level);
        }
    }

    private void parseLevels(File absolutePath) throws IOException {
        if(!absolutePath.exists()) {
            throw new IllegalStateException("levels.txt should exist.");
        }

        String text;

        text = FileUtils.readFileToString(absolutePath, Charset.defaultCharset());

        parseLevels(text);
    }

    HashMap<DelpRule, Integer> getRuleMap() {
        HashMap<DelpRule, Integer> delpToInt = new HashMap<>(stringRuleToLevel.size());
        DefeasibleLogicProgram joined = BlackUtilsKt.join(firstplayers, secondplayers);
        DelpParser parser = new DelpParser();

        stringRuleToLevel.forEach((s, integer) -> {
            DefeasibleLogicProgram parsedDb = null;

            // Try and parse the formula of which we know the level
            try {
                parsedDb = parser.parseBeliefBase(s);
            } catch (IOException e) {
                e.printStackTrace();
                Logger.INSTANCE.e(id, e.toString());
            }

            DelpRule rule = parsedDb.iterator().next();

            // Try and find the matching rule
            Optional<DelpRule> foundRule = Optional.empty();
            for (DelpRule r1 : joined) {
                if (r1.equals(rule)) {
                    foundRule = Optional.of(r1);
                    break;
                }
            }

            // If the matching rule exists, then put it into memory.
            if(!foundRule.isPresent()) {
                Logger.INSTANCE.w(id, MessageFormat.format("Rule {0} from levels.txt was ignored.", s));
            } else {
                delpToInt.put(foundRule.get(), integer);
            }
        });

        return delpToInt;
    }

    public void reparseInitial(String text) throws IOException {
        initialquestion = parseInital(text, new DelpParser());
    }

    private DelpLocution parseInital(String text, Parser<DefeasibleLogicProgram> parser) throws IOException {
        if (text.contains("-<") || text.contains("->")) {
            // Assume a rule

            // Parse and get the first (and only) rule.
            DelpRule rule = parser.parseBeliefBase(new StringReader(text)).iterator().next();

            return new DelpLocution(rule);
        } else {
            // Assume a proposition
            DelpFact rule = (DelpFact) parser.parseBeliefBase(new StringReader(text)).iterator().next();

            return new DelpLocution(rule.getConclusion());
        }
    }

    @Override
    public DelpLocution parseInitial(File absolutePath, Parser<DefeasibleLogicProgram> parser) {
        String text = null;

        try {
            text = FileUtils.readFileToString(absolutePath, Charset.defaultCharset());

            return parseInital(text, parser);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}