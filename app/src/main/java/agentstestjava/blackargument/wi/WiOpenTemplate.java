/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.wi;


import agentlib.AgentId;
import agentlib.argumentation.ArgumentationAgent;
import agentlib.argumentation.IMove;
import agentlib.argumentation.Move;
import agentlib.argumentation.movehistory.MoveHistory;
import agentlib.argumentation.movehistory.MoveHistoryTemplate;
import agentstestjava.DialogueTypes;
import agentstestjava.Roles;
import agentstestjava.blackargument.BlackUtilsKt;
import agentstestjava.blackargument.DelpLocution;
import net.sf.tweety.arg.delp.DefeasibleLogicProgram;
import net.sf.tweety.arg.delp.syntax.DelpRule;
import net.sf.tweety.logics.fol.syntax.FolFormula;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WiOpenTemplate extends MoveHistoryTemplate<DelpLocution, Roles, DefeasibleLogicProgram, FolFormula> {
    public WiOpenTemplate(Roles[] applicableForRoles) {
        super(applicableForRoles);
    }

    @Override
    protected boolean isApplicableInternal(MoveHistory<DelpLocution, FolFormula> history, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> hisTurn, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> addressedTo, Roles roleOfTurnTaker) {
        return history.getLength() == 0;
    }

    @NotNull
    @Override
    protected List<IMove<DelpLocution>> getMovesInternal(MoveHistory<DelpLocution, FolFormula> history, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> hisTurn, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> addressedTo, Roles roleOfTurnTaker) {
        List<DelpRule> list = new ArrayList<>();

        if (history.getLength() == 0) {
            list.add(hisTurn.getInitialQuestion().getRule());
        }

        // TODO: Filter NOT depending on player!
        return list.stream()
                .map(r1 -> new WiOpen(hisTurn.getId(), addressedTo.getId(), new DelpLocution(r1)))
                .filter(r1 -> !history.contains(r1))
                .collect(Collectors.toList());
    }

    @Override
    protected void applyInternal(MoveHistory<DelpLocution, FolFormula> history, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> hisTurn, IMove<DelpLocution> moveToApply) {
        // Open subdialogue with specific topic
        history.openSubdialogue(DialogueTypes.BLACK_WARRANT_INQUIRY, false);

        // Fill the topic with the argument's things.
        for (FolFormula formula : BlackUtilsKt.getLiteralsOf(moveToApply.getPropositions().get(0).getRule())) {
            if (!formula.toString().equals("TRUE")) {
                history.addToCurrentTopic(formula);
            }
        }

    }

    public class WiOpen extends Move<DelpLocution> {
        public WiOpen(AgentId from, AgentId addressedTo, DelpLocution proposition) {
            super(from, addressedTo, proposition);

            if (!proposition.isInferenceRule()) {
                throw new IllegalArgumentException();
            }
        }

        @Override
        public Move<DelpLocution> clone() {
            return new WiOpen(getFrom(), getAddressedTo(), getPropositions().get(0));
        }

        @Override
        public String getName() {
            return "WiOpen";
        }
    }
}
