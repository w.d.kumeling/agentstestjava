/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava;

import net.sf.tweety.commons.BeliefBase;
import net.sf.tweety.commons.Parser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/***
 * Given a directory, it parses the txt files into beliefbases
 * @param <T>
 */
public abstract class BeliefBaseParser<T extends BeliefBase, T2> {
    public T firstplayers, secondplayers;
    public T2 initialquestion;
    protected Parser<T> parser;

    public BeliefBaseParser(Parser<T> parser, String path) throws IOException {
        this.parser = parser;

        File dir = new File(path);

        if (!Files.exists(dir.toPath()) || !Files.isDirectory(dir.toPath())) {
            throw new IllegalArgumentException("Path should be a directory");
        }

        File first, second, initial;

        first = new File(dir, "firstplayer.txt");
        second = new File(dir, "secondplayer.txt");
        initial = new File(dir, "initial.txt");

        if (!first.exists() || !second.exists() || !initial.exists()) {
            throw new IllegalStateException("All files should exist and have the right names: firstplayer.txt, secondplayer.txt, initial.txt");
        }

        // Let's parse first and second, and leave initial over to a subclass.
        firstplayers = parser.parseBeliefBaseFromFile(first.getAbsolutePath());
        secondplayers = parser.parseBeliefBaseFromFile(second.getAbsolutePath());

        initialquestion = parseInitial(initial, parser);
    }

    public void reparseFirstPlayer(String text, Parser<T> parser) throws IOException {
        firstplayers = parser.parseBeliefBase(text);
    }

    public void reparseSecondPlayer(String text, Parser<T> parser) throws IOException {
        secondplayers = parser.parseBeliefBase(text);
    }

    public abstract T2 parseInitial(File absolutePath, Parser<T> parser);
}
