/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.types.path
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import java.io.StringWriter

class ReadLogs : CliktCommand(name = "readlogs", help = "Helps to output a csv containing some data from ran data sets.") {
    val pathToDatasets by argument().path(exists = true, fileOkay = false, readable = true, writable = false)

    override fun run() {
        val rootargregex = """Root argument:\n=+\n(.+)\n""".toRegex()
        val statusofrootregex = """Status of root:\n=+\n(.+)\n""".toRegex()
        val numberofrulesregex = """Number of rules:\n=+\n(.+)\n""".toRegex()
        val numberofargumentsregex = """Number of arguments:\n=+\n(.+)\n""".toRegex()
        val argumentsinpublicspaceregex = """Arguments in public space:\n=+\n(.+)\n""".toRegex()
        val commitmenttreeregex = """Commitment tree:\n=+\n+([\w \{\n\"\<\!\-\.\,\}\>\;]+})""".toRegex()
        val fulltreeregex = """Full tree:\n=+\n+([\w \{\n\"\<\!\-\.\,\}\>\;]+})""".toRegex()
        val historyregex = """Dumping history:\n=+([\w \{\n\"\<\!\-\.\,\}\>\;\:\[\]]+)""".toRegex()

        val directories = pathToDatasets.toFile().walkTopDown()
                .filter { it.isDirectory }
                .filter { it != pathToDatasets.toFile() }

        val writer = StringWriter()

        val tsv = CSVPrinter(writer, CSVFormat.TDF.withHeader(
                "Dataset",
                "N",
                "Strategy",
                "BHTree",
                "RootArg",
                "StatusOfRoot",
                "NumberOfRules",
                "NumberOfArgs",
                "Argsinpublicspace",
                "commitmenttree",
                "fulltree",
                "sizeOfHistory",
                "sizeOfTree"))

        for (directory in directories) {
            val files = directory.listFiles { _, name -> name.startsWith("log") }
            val dataset = directory.name.split('_')
            val datasetName = dataset[0]
            val n = dataset[1].toInt()

            for (file in files) {
                val items = file.nameWithoutExtension.split('_')
                val strategyUsed = items[1]
                val bhTreeUsed = items[3]

                val text = file.readLines()
                        .asSequence()
                        .filter { it.startsWith("STDO\\I: ") }
                        .map { it.removePrefix("STDO\\I: ") }
                        .reduce { acc, s -> acc + "\n" + s }

                val rootarg = rootargregex.find(text)!!.groupValues[1]
                val statusofroot = statusofrootregex.find(text)!!.groupValues[1]
                val numberofrules = numberofrulesregex.find(text)!!.groupValues[1].toInt()
                val numberofarguments = numberofargumentsregex.find(text)!!.groupValues[1].toInt()
                val argumentsinpublicspace = argumentsinpublicspaceregex.find(text)!!.groupValues[1].toInt()
                val commitmenttree = commitmenttreeregex.find(text)!!.groupValues[1]
                val fulltree = fulltreeregex.find(text)!!.groupValues[1]
                val history = historyregex.find(text)!!.groupValues[1]
                val sizeOfHistory = history.split('\n').size
                // Size of tree measured in the number of arguments present in the tree
                // The number of arguments present is the number of attacks
                // Plus 1 to compensate for the root argument that attacks noone
                val sizeOfTree: Int = fulltree.split('\n').size - 2 + 1

                tsv.printRecord(
                        datasetName,
                        n,
                        strategyUsed,
                        bhTreeUsed,
                        rootarg,
                        statusofroot,
                        numberofrules,
                        numberofarguments,
                        argumentsinpublicspace,
                        commitmenttree,
                        fulltree,
                        sizeOfHistory,
                        sizeOfTree)
            }
        }

        tsv.close(true)

        pathToDatasets.resolve(".\\result.tsv")
                .toFile()
                .writeText(writer.toString())
    }
}