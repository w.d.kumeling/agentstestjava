/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.cli

import agentlib.Logger
import agentstestjava.blackargument.StringLogger
import agentstestjava.gui.MainApp
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.optional
import com.github.ajalt.clikt.parameters.types.path
import tornadofx.launch
import java.nio.file.Path

class Ui : CliktCommand(name = "gui") {
    val pathToDataset: Path? by argument(help = "The path to the dataset to open")
            .path(exists = true, folderOkay = true, fileOkay = false, readable = true)
            .optional()

    override fun run() {
        Logger.sink = StringLogger()

        launch<MainApp>(pathToDataset?.toAbsolutePath().toString())
    }

}