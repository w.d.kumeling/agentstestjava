/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava

import agentlib.AgentId
import agentlib.Logger
import agentlib.argumentation.*
import agentstestjava.blackargument.BlackHistory
import java.util.*

class AskStrategy<T : ILocution> : IStrategy<T> {
    private var s = Scanner(System.`in`)

    override fun chooseMove(moves: Collection<IMove<T>>,
                            history: IDialogueHistory<T>,
                            otherAgent: ArgumentationAgent<T, *, *>,
                            extra: ExtraInformation): Optional<IMove<T>> {

        val hist = history as BlackHistory
        Logger.i(id, "Current tree: ${hist.tree}")

        if (moves.isEmpty()) {
            return Optional.empty()
        } else {
            val sb = StringBuilder()

            sb.append("\nChoose one of the following:\n")

            for ((i, iMove) in moves.withIndex()) {
                sb.append("${i+1}: $iMove\n")
            }

            println(sb)

            while (true) {
                print("Which move? (0 is none) ")
                System.out.flush()
                val line = s.nextLine()

                try {
                    return when (line) {
                        "0" -> Optional.empty()
                        else -> Optional.of(moves.toTypedArray()[Integer.valueOf(line) - 1])
                    }
                } catch (e: NumberFormatException) {
                }
            }
        }

    }

    companion object {
        private val id = AgentId("ASK")
    }
}
