/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava

import java.util.*
import kotlin.math.pow

object DatasetGenerator {
    data class Dataset(val firstplayer: String, val secondplayer: String, val levels: String, val initial: String)

    fun getAmbiguityHandling(n: Int): Dataset {
        val first = StringBuilder()
        val second = StringBuilder()
        val levels = StringBuilder()

        first.append("q -< s$n.\n")
        first.append("~q -< p$n.\n")

        first.append("~p${n / 2} -< r${n / 2}.\n")

        second.append("s$n -< TRUE.\n")
        second.append("p$n -< TRUE.\n")

        second.append("r${n / 2} -< TRUE.\n")

        for (i in (n - 1) downTo 1) {
            first.append("~s${i + 1} -< s$i.\n")
            first.append("~p${i + 1} -< p$i.\n")

            second.append("s$i -< TRUE.\n")
            second.append("p$i -< TRUE.\n")
        }

        for (i in (n / 2 - 1) downTo 1) {
            first.append("~r${i + 1} -< r$i.\n")

            second.append("r$i -< TRUE.\n")
        }

        for (item in first.toString().split("\n")) {
            if (item.isNotBlank())
                levels.append("$item; 1\n")
        }

        for (item in second.toString().split("\n")) {
            if (item.isNotBlank())
                levels.append("$item; 1\n")
        }

        first.append("TRUE.")
        second.append("TRUE.")

        return Dataset(first.toString(), second.toString(), levels.toString(), "q -< TRUE.")
    }

    fun getFloatingConclusionsData(n: Int): Dataset {
        val firstsb = StringBuilder()
        val secondsb = StringBuilder()
        val levelsb = StringBuilder()


        for (i in 1..n) {
            firstsb.append("q -< p$i.\n")
            secondsb.append("p$i -< TRUE.\n")

            firstsb.append("q -< ~p$i.\n")
            secondsb.append("~p$i -< TRUE.\n")
        }

        for (item in firstsb.toString().split("\n")) {
            if (item.isNotBlank())
                levelsb.append("$item; 1\n")
        }

        for (item in secondsb.toString().split("\n")) {
            if (item.isNotBlank())
                levelsb.append("$item; 1\n")
        }

        firstsb.append("TRUE.")
        secondsb.append("TRUE.")

        return Dataset(firstsb.toString(), secondsb.toString(), levelsb.toString(), "q -< TRUE.")
    }

    fun getTeamDefeatCascading(n: Int): Dataset {
        val first = StringBuilder()
        val second = StringBuilder()
        val levels = StringBuilder()

        val stack = ArrayDeque<String>(n * n)
        var globi = 0

        stack.addLast("p$globi")

        globi++

        for (i in 0 until n) {
            for (j in 1..(2.0).pow(i).toInt().times(2)) {
                val el: String = stack.pop()

                if (!el.startsWith('~')) {
                    stack.addLast("~$el")
                } else {
                    stack.addLast(el.removePrefix("~"))
                }

                for (ik in 1..2) {
                    first.append("$el -< p$globi.\n")
                    second.append("p$globi -< TRUE.\n")

                    levels.append("$el -< p$globi.; ${i + 1}\n")
                    levels.append("p$globi -< TRUE.; ${i + 1}\n")

                    stack.addLast("~p$globi")

                    globi++
                }

            }

        }

        first.append("TRUE.")
        second.append("TRUE.")

        return Dataset(first.toString(), second.toString(), levels.toString(), "p0 -< TRUE.")
    }

    private inline infix fun String.comesFrom(to: String): String {
        return "$this -< $to.\n"
    }

    private inline infix fun String.level(level: Int): String {
        return "${this.removeSuffix("\n")}; $level\n";
    }

    private inline fun String.negate(): String =
            if (this.startsWith('~')) this.removePrefix("~") else "~$this"

    // Nu hebben alle argumenten nog dezelfde level
    fun getTeamDefeatDataset(n: Int): Dataset {
        val first = StringBuilder()
        val second = StringBuilder()
        val levels = StringBuilder()

        val stack = ArrayDeque<String>(n * n)
        var globi = 0

        stack.addLast("p0")
        globi++

        first.append("p0" comesFrom "p$globi")
        second.append("p0" comesFrom "TRUE")

        levels.append("p0" comesFrom "p$globi" level 1)
        levels.append("p0" comesFrom "TRUE" level 1)

        globi++

        for (j in 1..((2.0).pow(n).toInt() - 1)) {
            val el: String = stack.pop()

            for (ik in 1..2) {
                var rule = el.negate() comesFrom "p$globi"
                var fact = "p$globi" comesFrom "TRUE"

                first.append(rule)
                second.append(fact)

                levels.append(rule level 1)
                levels.append(fact  level 1)

                globi++

                rule = "p${globi-1}".negate() comesFrom "p$globi"
                fact = "p$globi" comesFrom "TRUE"

                first.append(rule)
                second.append(fact)

                levels.append(rule level 1)
                levels.append(fact level 1)

                stack.addLast("p$globi")

                globi++
            }

        }

        first.append("TRUE.")
        second.append("TRUE.")

        return Dataset(first.toString(), second.toString(), levels.toString(), "p0" comesFrom "TRUE")
    }
}