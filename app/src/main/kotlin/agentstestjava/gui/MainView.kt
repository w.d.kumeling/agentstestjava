/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.gui

import javafx.scene.control.ToggleGroup
import javafx.scene.layout.AnchorPane
import tornadofx.*
import tornadofx.controlsfx.popover
import tornadofx.controlsfx.showPopover

class MainView : View() {
    private val controller: MainController by inject()

    override val root: AnchorPane = anchorpane {

        tabpane {

            anchorpaneConstraints {
                topAnchor = 0
                leftAnchor = 0
                rightAnchor = 0
                bottomAnchor = 0
            }

            tab("Facts") {
                closableProperty().set(false)

                gridpane {


                    label("Proponent KB: ").gridpaneConstraints {
                        columnIndex = 0
                        rowIndex = 0
                    }
                    textarea(controller.dbOfFirst).gridpaneConstraints {
                        columnIndex = 0
                        rowIndex = 1
                    }

                    label("Opponent KB: ").gridpaneConstraints {
                        columnIndex = 0
                        rowIndex = 2
                    }
                    textarea(controller.dbOfSecond).gridpaneConstraints {
                        columnIndex = 0
                        rowIndex = 3
                    }

                    label("Initial question: ").gridpaneConstraints {
                        columnIndex = 0
                        rowIndex = 4
                    }
                    textarea(controller.initialQuestion).gridpaneConstraints {
                        columnIndex = 0
                        rowIndex = 5
                    }

                    vbox {
                        gridpaneConstraints {
                            columnIndex = 1
                            rowIndex = 0
                            rowSpan = 2
                        }

                        style {
                            paddingRight = 8
                        }

                        label("Proponent's strategy:")
                        radiobutton("Exhaustive strategy", controller.firstPersonToggleGroup, "exhaust") {
                            isSelected = true
                        }
                        radiobutton("Limited Dialogue", controller.firstPersonToggleGroup, "limiteddiag")
                        radiobutton("Limited Commitment", controller.firstPersonToggleGroup, "limitedcom")
                        radiobutton("Smart Original", controller.firstPersonToggleGroup, "smartorig")
                        radiobutton("Smart Dialogue", controller.firstPersonToggleGroup, "smartdiag")
                    }

                    vbox {
                        gridpaneConstraints {
                            columnIndex = 1
                            rowIndex = 2
                            rowSpan = 2
                        }

                        label("Opponent's strategy:")
                        radiobutton("Exhaustive strategy", controller.secondPersonToggleGroup, "exhaust") {
                            isSelected = true
                        }
                        radiobutton("Limited Dialogue", controller.secondPersonToggleGroup, "limiteddiag")
                        radiobutton("Limited Commitment", controller.secondPersonToggleGroup, "limitedcom")
                        radiobutton("Smart Original", controller.secondPersonToggleGroup, "smartorig")
                        radiobutton("Smart Dialogue", controller.secondPersonToggleGroup, "smartdiag")
                    }

                    button("Choose different example") {
                        gridpaneConstraints {
                            columnIndex = 1
                            rowIndex = 4
                        }
                    }.action { controller.chooseDifferentDirectory() }
                }
            }
            tab("Dialogue") {
                closableProperty().set(false)

                gridpane {

                    label("History").gridpaneConstraints {
                        columnIndex = 0
                        rowIndex = 0
                    }
                    textarea(controller.history) {
                        gridpaneConstraints {
                            columnIndex = 0
                            rowIndex = 1
                            rowSpan = 3
                        }
                        isEditable = false
                    }

                    label("Current legal moves:").gridpaneConstraints {
                        columnIndex = 0
                        rowIndex = 4
                    }
                    textarea(controller.legalMoves) {
                        gridpaneConstraints {
                            columnIndex = 0
                            rowIndex = 5
                        }
                        isEditable = false
                    }

                    hbox {
                        gridpaneConstraints {
                            columnIndex = 1
                            rowIndex = 0
                            rowSpan = 2
                        }

                        children.style {
                            padding = box(5.px)
                        }

                        vbox {
                            style {
                                padding = box(5.px)
                            }

                            button("Initialise/Reset").action { controller.initializePlatform() }
                            button("Run").action { controller.run() }
                            button("Step").action { controller.step() }
                            //button("Show legal moves").action { controller.showLegalMoves() }
                        }
                    }

                    label("Current tree").gridpaneConstraints {
                        columnIndex = 1
                        rowIndex = 2
                    }
                    imageview(controller.currentTree) {
                        gridpaneConstraints {
                            columnIndex = 1
                            rowIndex = 3
                        }
                    }
                }
            }
        }

    }
}

