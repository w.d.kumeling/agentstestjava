/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava

import agentstestjava.blackargument.BetterDiacTree
import agentstestjava.blackargument.IBetterDiacTree
import net.sf.tweety.arg.delp.syntax.DelpArgument
import java.util.*

class DiacTreeDot(val root: IBetterDiacTree) {
    private val simplifyArgMap: MutableMap<DelpArgument, String> = linkedMapOf()

    fun getJson(): String {
        val queue = ArrayDeque<IBetterDiacTree>()

        val attacks = mutableListOf<String>()
        val propositions = mutableListOf<String>()

        queue.addLast(root)

        while (queue.isNotEmpty()) {
            val elem = queue.pop()

            queue.addAll(elem.children)

            for (attacker in elem.children) {
                attacks.add(getAttack(elem.argument, attacker.argument))
            }

            propositions.add(getArgumentItself(elem.argument))
        }

        return formatDot(attacks, propositions)
    }

    private fun formatDot(attacks: List<String>, propositions: List<String>): String {
        val full = StringBuilder()

        full.appendln("digraph hello {")

        for (attack in attacks) {
            full.appendln(attack)
        }

        //full.append("},")

        //full.append("\"propositions\":{")

        //for (prop in propositions) {
        //    full.append("\"$prop\",")
        //}

        full.append("}")

        return full.toString()
    }

    private fun getArgumentItself(argument: DelpArgument): String {
        return simplifyArgument(argument)
    }

    private fun getAttack(attacked: DelpArgument, attacker: DelpArgument): String {
        return "\"${simplifyArgument(attacker)}\" -> \"${simplifyArgument(attacked)}\";"
    }

    private var globi = 0;
    private fun simplifyArgument(argument: DelpArgument): String {
        if (argument !in simplifyArgMap) {
            simplifyArgMap[argument] = argument.toString().replace(" ","")
        }

        return simplifyArgMap[argument]!!
    }
}