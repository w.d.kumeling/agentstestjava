/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument

import agentlib.argumentation.*
import agentstestjava.Roles
import net.sf.tweety.arg.delp.DefeasibleLogicProgram

class DelpAgent(roleOfAgent: Roles,
                strat: IStrategy<DelpLocution>,
                assertion: AssertionAttitude<DefeasibleLogicProgram, DelpLocution>,
                acceptance: AcceptanceAttitude<DefeasibleLogicProgram, DelpLocution>) :
        ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>(roleOfAgent, strat, acceptance, assertion) {

    override val extraInformationForStrategy: ExtraInformation
        get() = DelpExtraInformation(beliefBase, commitmentStore, roleOfAgent)

    init {
        beliefBase = DefeasibleLogicProgram()
        commitmentStore = DefeasibleLogicProgram()
    }

    fun getKnowledge(otherAgents: DefeasibleLogicProgram): DefeasibleLogicProgram {
        return join(beliefBase, commitmentStore, otherAgents)
    }

    override fun reset() {
        commitmentStore.clear()
    }

    inner class DelpExtraInformation(val beliefBaseOfTurnTaker: DefeasibleLogicProgram,
                                     val commitmentStoreOfTurnTaker: DefeasibleLogicProgram,
                                     val myOwnRole: Roles) : ExtraInformation()
}


