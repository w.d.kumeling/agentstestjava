/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package agentstestjava.blackargument

import net.sf.tweety.arg.delp.DefeasibleLogicProgram
import net.sf.tweety.arg.delp.semantics.ComparisonCriterion
import net.sf.tweety.arg.delp.syntax.DelpArgument
import java.util.*


/**
 * This class implements a node of a dialectical tree from DeLP.
 * Modified from the Tweety 1.10 implementation, to fix getDefeaters in order to be able to incrementally build
 * the tree, based on the asserts in the dialogue.
 * @author Matthias Thimm
 * @author Wouter Kumeling
 */
open class BetterDiacTree(
        /**
         * The parent node; <source></source>null if this node is a root
         */
        override val parent: IBetterDiacTree?,
        /**
         * The argument in this node
         */
        override val argument: DelpArgument) : IBetterDiacTree {

    /**
     * The children of this node; size=0 indicates a leaf
     */
    override val children: MutableSet<IBetterDiacTree> = HashSet()


    /**
     * constructor; initializes this dialectical tree node as a root with given argument
     *
     * @param argument a DeLP argument
     */
    constructor(argument: DelpArgument) : this(null, argument)

    /**
     * Computes the set of arguments which are defeaters for the argument in this tree node and returns
     * the corresponding dialectical tree nodes with these defeaters. For the computation of the defeaters
     * the whole argumentation line to this tree node is considered. As a side effect the computed tree nodes
     * are added as children of this node. The set of children is cleared first if desired. Also, optionally, all
     * arguments ever moved can be considered.
     *
     * @param arguments           a set of arguments
     * @param delp                a defeasible logic program
     * @param comparisonCriterion a comparison criterion.
     * @param addAsChild          whether to add new nodes to the tree.
     * @param getArgumentsFromDlpToo whether to also get the arguments from [delp]
     * @return the set of new defeater nodes of the argument in this node
     */
    override fun getNewDefeaters(arguments: Set<DelpArgument>,
                                 delp: DefeasibleLogicProgram,
                                 comparisonCriterion: ComparisonCriterion,
                                 addAsChild: Boolean,
                                 getArgumentsFromDlpToo: Boolean): Set<IBetterDiacTree> {

        if (getArgumentsFromDlpToo) {
            throw IllegalStateException("If using a bhtree, use BHBetterDiacTree.")
        }

        val args = mutableSetOf<DelpArgument>()
        val argumentsOfChildren = children.map { it.argument }.toSet()

        args.addAll(arguments)

        if (getArgumentsFromDlpToo) {
            args.addAll(delp.arguments)
        }

        val defs = argument.getAttackOpportunities(delp) //gather attacks of last argument in the line
                .flatMap { lit -> args.filter { argument -> argument.conclusion == lit } } // Filter arguments whose conclusion attack
                .asSequence()
                .filter { isAcceptable(it, delp, comparisonCriterion) } //for each attacker check acceptability
                .filter { it !in argumentsOfChildren } // Eliminate arguments already a child
                .map { BetterDiacTree(this, it) } // Build dialectical tree
                .toSet() // Eliminate duplicates

        if (addAsChild) {
            children.addAll(defs)
        }

        return defs
    }

    override fun toString(): String {
        return "[$argument${if (children.isNotEmpty()) " - " else ""}${children.joinToString(transform = IBetterDiacTree::toString)}]"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as BetterDiacTree?

        val queue = LinkedList<Pair<IBetterDiacTree, IBetterDiacTree>>()

        queue.add(this to that!!)

        while (queue.isNotEmpty()) {
            val item = queue.pop()

            if (item.first.argument != item.second.argument ||
                    item.first.children.size != item.second.children.size) {
                return false
            } else {
                val firstchild = item.first.children.iterator()
                val secondchild = item.second.children.iterator()

                while (firstchild.hasNext()) {
                    queue.add(firstchild.next() to secondchild.next())
                }
            }
        }

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(children, argument)
    }

}
