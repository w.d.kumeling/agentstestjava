/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument

import agentlib.argumentation.IDialogueHistory
import agentlib.argumentation.Protocol
import agentstestjava.Roles
import net.sf.tweety.arg.delp.DefeasibleLogicProgram

class BlackProtocol : Protocol<DelpLocution, Roles, DefeasibleLogicProgram>() {
    override fun dialogueShouldEnd(history: IDialogueHistory<DelpLocution>): Boolean {
        val history1 = history as BlackHistory

        val parentTopic = history1.parentTopic

        // Terminate upon matched-close AND if no parent dialogue exists.
        return history1.matchedClose() && !parentTopic.isPresent

    }

}


