/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.strategies

import agentlib.argumentation.*
import agentstestjava.blackargument.BlackHistory
import agentstestjava.blackargument.DelpLocution
import agentstestjava.blackargument.ai.AssertTemplate
import java.util.*

/**
 * Limited strategy which checks in the dialogue whether an argument is assertable
 */
class LimitedDialogueExhaustiveStrategy : IStrategy<DelpLocution> {

    override fun chooseMove(moves: Collection<IMove<DelpLocution>>,
                            history: IDialogueHistory<DelpLocution>,
                            otherAgent: ArgumentationAgent<DelpLocution, *, *>,
                            extra: ExtraInformation): Optional<IMove<DelpLocution>> {
        val movs = moves.sortedWith(comp)
        val hist = history as BlackHistory

        // Get which propositions already have an argument in the dialogue
        val conclusions = hist.moves
                .asSequence()
                .filter { it is AssertTemplate.Assert }
                .map { it.propositions[0].argument!!.conclusion }
                .toSet()

        for (move in movs) {
            if(move is AssertTemplate.Assert) {
                assert(move.propositions[0].isArgument)

                if(move.propositions[0].argument!!.conclusion in conclusions) {
                    continue
                }
            }

            return Optional.of(move)
        }

        return Optional.empty()
    }

    companion object {
        private val comp = ExhaustiveMoveComparator
    }
}
