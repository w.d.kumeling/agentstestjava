/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.withsupport

import net.sf.tweety.arg.delp.DefeasibleLogicProgram
import net.sf.tweety.arg.delp.semantics.ComparisonCriterion
import net.sf.tweety.arg.delp.syntax.DelpArgument
import net.sf.tweety.arg.delp.syntax.DelpRule
import java.util.*

class SupportLevelComparison : ComparisonCriterion() {
    private val ruleToSupportLevel: Dictionary<DelpRule, Int>

    init {
        ruleToSupportLevel = Hashtable()
    }

    fun add(rule: DelpRule, level: Int) {
        ruleToSupportLevel.put(rule, level)
    }

    operator fun get(argument: DelpArgument): Int? {
        return argument.support
                .map { ruleToSupportLevel[it] }
                .min()
    }

    operator fun get(rule: DelpRule): Int {
        return ruleToSupportLevel[rule]
    }

    override fun compare(argument1: DelpArgument?, argument2: DelpArgument?, context: DefeasibleLogicProgram): ComparisonCriterion.Result {
        if (argument1 == null || argument2 == null) {
            return ComparisonCriterion.Result.NOT_COMPARABLE
        }

        val arg1 = get(argument1)
        val arg2 = get(argument2)

        return when {
            arg1 == null || arg2 == null -> ComparisonCriterion.Result.NOT_COMPARABLE
            arg1 > arg2                  -> ComparisonCriterion.Result.IS_WORSE
            arg1 < arg2                  -> ComparisonCriterion.Result.IS_BETTER
            arg1 == arg2                 -> ComparisonCriterion.Result.IS_EQUAL
            else                         -> ComparisonCriterion.Result.NOT_COMPARABLE
        }
    }
}
