/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument

import agentlib.argumentation.ILocution
import agentlib.argumentation.IMove
import agentlib.argumentation.movecheckers.MoveEqualsChecker
import agentstestjava.blackargument.ai.CloseTemplate
import java.util.*

class BlackMoveEqualCheck<T : ILocution> : MoveEqualsChecker<T> {
    private val locutionEquals = HashMap<String, Array<String>>()

    init {
        locutionEquals["Close"] = arrayOf("Close")
        locutionEquals["Assert"] = arrayOf("Assert")
        locutionEquals["AiOpen"] = arrayOf("AiOpen", "OpenAiInAI", "OpenAiInWi", "WiOpen")
        locutionEquals["OpenAiInAI"] = arrayOf("AiOpen", "OpenAiInAI", "OpenAiInWi", "WiOpen")
        locutionEquals["OpenAiInWi"] = arrayOf("AiOpen", "OpenAiInAI", "OpenAiInWi", "WiOpen")
        locutionEquals["WiOpen"] = arrayOf("AiOpen", "OpenAiInAI", "OpenAiInWi", "WiOpen")
    }

    override fun moveEquals(one: IMove<T>, other: IMove<T>): Boolean {
        if (one === other) return true

        // First check propositions, if they don't match, then the moves don't match either.
        if (one.propositions.toString() != other.propositions.toString()) {
            return false
        }

        // In case of Close moves, check the topic instead
        if (one is CloseTemplate.Close && other is CloseTemplate.Close) {
            if ((one as CloseTemplate.Close).topic != (other as CloseTemplate.Close).topic) {
                return false
            }
        }

        // Then check whether the name of the moves correspond.
        for (s in locutionEquals[one.name]!!) {
            if (s == other.name) {
                return true
            }
        }

        return false
    }
}