/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.ai

import agentlib.AgentId
import agentlib.argumentation.ArgumentationAgent
import agentlib.argumentation.IMove
import agentlib.argumentation.Move
import agentlib.argumentation.movehistory.MoveHistory
import agentlib.argumentation.movehistory.MoveHistoryTemplate
import agentstestjava.DialogueTypes
import agentstestjava.Roles
import agentstestjava.blackargument.DelpLocution
import agentstestjava.blackargument.getLiteralsOf
import net.sf.tweety.arg.delp.DefeasibleLogicProgram
import net.sf.tweety.arg.delp.syntax.DelpRule
import net.sf.tweety.logics.fol.syntax.FolFormula
import java.util.*

class AiOpenTemplate(applicableForRoles: Array<Roles>) : MoveHistoryTemplate<DelpLocution, Roles, DefeasibleLogicProgram, FolFormula>(applicableForRoles) {

    override fun isApplicableInternal(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, addressedTo: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, roleOfTurnTaker: Roles): Boolean {
        return history.length == 0
    }

    override fun getMovesInternal(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, addressedTo: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, roleOfTurnTaker: Roles): List<IMove<DelpLocution>> {
        val list = ArrayList<DelpRule>()

        if (hisTurn.initialQuestion.isInferenceRule) {
            list.add(hisTurn.initialQuestion.rule!!)
        }

        return list
                .map { r1 -> AiOpen(hisTurn.id, addressedTo.id, DelpLocution(r1)) }
                .filter { r1 -> !history.contains(r1) }
                .toList()
    }

    override fun applyInternal(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, moveToApply: IMove<DelpLocution>) {
        // Open subdialogue with specific topic
        history.openSubdialogue(DialogueTypes.BLACK_ARGUMENT_INQUIRY, false)

        // Fill the topic with the argument's things.
        for (formula in moveToApply.propositions[0].rule!!.getLiteralsOf()) {
            if (formula.toString() != "TRUE") {
                history.addToCurrentTopic(formula)
            }
        }

    }

    inner class AiOpen(from: AgentId, addressedTo: AgentId, proposition: DelpLocution) : Move<DelpLocution>(from, addressedTo, proposition) {

        override val name: String
            get() = "AiOpen"

        init {

            if (!proposition.isInferenceRule) {
                throw IllegalArgumentException()
            }
        }

        override fun clone(): Move<DelpLocution> {
            return AiOpen(from, addressedTo, propositions[0])
        }

    }

}
