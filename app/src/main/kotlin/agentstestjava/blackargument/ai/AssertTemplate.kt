/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.ai

import agentlib.AgentId
import agentlib.argumentation.ArgumentationAgent
import agentlib.argumentation.IMove
import agentlib.argumentation.Move
import agentlib.argumentation.movehistory.MoveHistory
import agentlib.argumentation.movehistory.MoveHistoryTemplate
import agentstestjava.DialogueTypes
import agentstestjava.Roles
import agentstestjava.blackargument.BlackHistory
import agentstestjava.blackargument.DelpAgent
import agentstestjava.blackargument.DelpLocution
import net.sf.tweety.arg.delp.DefeasibleLogicProgram
import net.sf.tweety.arg.delp.syntax.DelpFact
import net.sf.tweety.logics.commons.syntax.Predicate
import net.sf.tweety.logics.fol.syntax.FOLAtom
import net.sf.tweety.logics.fol.syntax.FolFormula
import java.util.*

class AssertTemplate(applicableForRoles: Array<Roles>) : MoveHistoryTemplate<DelpLocution, Roles, DefeasibleLogicProgram, FolFormula>(applicableForRoles) {

    override fun isApplicableInternal(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, addressedTo: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, roleOfTurnTaker: Roles): Boolean {
        return history.currentTopic.size > 0
    }

    override fun getMovesInternal(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, addressedTo: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, roleOfTurnTaker: Roles): List<IMove<DelpLocution>> {
        return when (history.currentDialogueType) {
            DialogueTypes.BLACK_ARGUMENT_INQUIRY -> getMovesAi(history, hisTurn, addressedTo, roleOfTurnTaker)
            DialogueTypes.BLACK_WARRANT_INQUIRY -> getMovesWi(history, hisTurn, addressedTo, roleOfTurnTaker)
            else -> throw IllegalStateException("Dont use this locution in an ${history.currentDialogueType} dialogue")
        }
    }


    private fun getMovesWi(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, addressedTo: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, roleOfTurnTaker: Roles): List<IMove<DelpLocution>> {
        // Get current topics
        val blackHistory = history as BlackHistory

        // Search for each topic corresponding supports
        val list = ArrayList<IMove<DelpLocution>>()
        val combined = (hisTurn as DelpAgent).getKnowledge(addressedTo.commitmentStore)
        val args = ArrayList(combined.arguments).filter { it.conclusion != TruePredicate }

        for (formula in history.currentTopic) {
            for (arg in args) {

                if (blackHistory.rootArgument == null && arg.conclusion == formula || blackHistory.rootArgument != null && blackHistory.changesTree(arg)) {
                    val move = Assert(hisTurn.id, addressedTo.id, DelpLocution(arg))

                    if (!history.contains(move)) {
                        list.add(move)
                    }
                }
            }
        }

        // Return for each support a move.
        return list
    }

    companion object {
        val TruePredicate = FOLAtom(Predicate("TRUE"))
    }

    private fun getMovesAi(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, addressedTo: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, roleOfTurnTaker: Roles): List<IMove<DelpLocution>> {
        // Get current topics

        // Search for each topic corresponding supports
        val list = ArrayList<IMove<DelpLocution>>()
        val combined = (hisTurn as DelpAgent).getKnowledge(addressedTo.commitmentStore)
        val args = ArrayList(combined.arguments).filter { it.conclusion != TruePredicate }

        for (formula in history.currentTopic) {
            for (arg in args) {

                if (arg.conclusion == formula && arg.support.size > 0) {


                    val move = Assert(hisTurn.id, addressedTo.id, DelpLocution(arg))

                    if (!history.contains(move)) {
                        list.add(move)
                    }
                }
            }
        }

        // Return for each support a move.
        return list
    }


    override fun applyInternal(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, moveToApply: IMove<DelpLocution>) {
        // not Remove conclusion from topic
        //history.removeFromCurrentTopic(moveToApply.getPropositions().get(0).getArgument().getConclusion());

        // DO add the argument to the commitment store
        val locution = moveToApply.propositions[0]
        val arg = locution.argument

        if (arg!!.support.size == 0) {
            // Assume that without a support, this is just the assertion of an atom.
            hisTurn.commitmentStore.add(DelpFact(arg.conclusion))
        } else {
            hisTurn.commitmentStore.addAll(
                    locution.argument!!.support
                            .filter { r -> r.toString() != "TRUE" }
                            .toSet())
        }
    }

    inner class Assert(from: AgentId, addressedTo: AgentId, proposition: DelpLocution) : Move<DelpLocution>(from, addressedTo, proposition) {

        override val name: String
            get() = "Assert"

        init {

            if (!proposition.isArgument) {
                throw IllegalArgumentException()
            }
        }

        override fun clone(): Move<DelpLocution> {
            return Assert(from, addressedTo, propositions[0])
        }
    }
}

