/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument

//
// Translated by CS2J (http://www.cs2j.com): 6-2-2018 14:22:39
//


import agentlib.argumentation.ILocution
import net.sf.tweety.arg.delp.syntax.DelpArgument
import net.sf.tweety.arg.delp.syntax.DelpRule
import net.sf.tweety.logics.fol.syntax.FolFormula
import java.util.*

class DelpLocution : ILocution {

    var argument: DelpArgument? = null
    var proposition: FolFormula? = null
    var rule: DelpRule? = null

    val isArgument: Boolean
        get() = argument != null && proposition == null && rule == null

    val isProposition: Boolean
        get() = proposition != null && argument == null && rule == null

    val isInferenceRule: Boolean
        get() = rule != null && argument == null && proposition == null

    constructor(arg: DelpArgument) {
        argument = arg
    }

    constructor(prop: FolFormula) {
        proposition = prop
    }

    constructor(rule: DelpRule) {
        this.rule = rule
    }

    constructor()

    override fun clone(): DelpLocution {
        // Let's do some funky magic to copy the proposition by value

        if (isArgument) {
            return DelpLocution(argument!!)
        } else if (isProposition) {
            return DelpLocution(proposition!!)
        } else if (isInferenceRule) {
            return DelpLocution(rule!!)
        }
        return DelpLocution()
    }

    override fun hashCode(): Int {
        return Objects.hash(argument, proposition, rule)
    }

    override fun toString(): String {
        return if (isProposition) {
            proposition!!.toString()
        } else if (isArgument) {
            argument!!.toString()
        } else if (isInferenceRule) {
            rule!!.toString()
        } else {
            ""
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DelpLocution

        if (argument != other.argument) return false
        if (proposition != other.proposition) return false
        if (rule != other.rule) return false

        return true
    }
}


