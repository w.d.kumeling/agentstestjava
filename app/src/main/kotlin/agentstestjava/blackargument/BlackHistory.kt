/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument

import agentlib.argumentation.IMove
import agentlib.argumentation.movehistory.MoveHistory
import agentstestjava.DialogueTypes
import agentstestjava.blackargument.ai.AssertTemplate
import net.sf.tweety.arg.delp.DefeasibleLogicProgram
import net.sf.tweety.arg.delp.semantics.ComparisonCriterion
import net.sf.tweety.arg.delp.semantics.EmptyCriterion
import net.sf.tweety.arg.delp.syntax.DelpArgument
import net.sf.tweety.arg.delp.syntax.DelpFact
import net.sf.tweety.logics.commons.syntax.Predicate
import net.sf.tweety.logics.fol.syntax.FOLAtom
import net.sf.tweety.logics.fol.syntax.FolFormula

import java.util.*

/**
 * @param criterion Specifies how arguments are compared. When used with [agentstestjave.blackargument.withsupport.SupportLevelComparison] supports preference levels
 * @param letTreeUseBlackAndHunter True iff the dialectical tree should follow the definition of Black and Hunter, i.e.
 * iff it should construct itself on the basis of the union of the commitment stores. False iff it instead should grow
 * incrementally
 */
class BlackHistory(var criterion: ComparisonCriterion = EmptyCriterion(),
                   val letTreeUseBlackAndHunter: Boolean = false,
                   var diacTreeGenerator: (parent: IBetterDiacTree?, arg: DelpArgument) -> IBetterDiacTree) : MoveHistory<DelpLocution, FolFormula>() {
    var argumentsMoved = DefeasibleLogicProgram()
    var rootArgument: DelpArgument? = null
    var tree: IBetterDiacTree? = null

    init {
        argumentsMoved.add(DelpFact(FOLAtom(Predicate("TRUE"))))
    }

    override fun reset() {
        super.reset()

        argumentsMoved.clear()
        rootArgument = null
        tree = null
    }

    override fun addMove(move: IMove<DelpLocution>) {
        super.addMove(move)

        // If the move is an assert then add to the above db.
        if (move is AssertTemplate.Assert) {
            val arg = move.propositions[0].argument!!

            // If the root argument is null, then add it, IF the conclusion is in the first wi topic
            // and only IF there exists a first warrant inquiry dialogue
            if (rootArgument == null) {
                val firstTopicOfDialogue = getFirstTopicOfDialogue(DialogueTypes.BLACK_WARRANT_INQUIRY)

                if (firstTopicOfDialogue.isPresent && firstTopicOfDialogue.get().contains(arg.conclusion)) {
                    rootArgument = arg
                    tree = diacTreeGenerator(null, rootArgument!!)
                }

            }
            argumentsMoved.addAll(arg.support)

            if (tree != null) {
                callGetDefeaters(this.tree!!, argumentsMoved, setOf(arg), criterion, true, letTreeUseBlackAndHunter)
            }
        }
    }

    data class ChangesStatusOfRootResult(val changesStatusOfRoot: Boolean, val changesTree: Boolean)

    fun changesStatusOfRoot(arg: DelpArgument): ChangesStatusOfRootResult {
        // The status of an empty tree is not defined.

        val oldMarking = tree!!.marking

        val front = LinkedList<IBetterDiacTree>()

        var argChangesTree = false

        front.add(tree!!)

        while (!front.isEmpty()) {
            val temp = front.remove()

            for (child in temp.children) {
                front.offer(child)
            }

            // Now, try and add the argument, and check whether it actually defeats it

            val newDefs = temp.getNewDefeaters(setOf(arg), argumentsMoved, criterion, true, letTreeUseBlackAndHunter)

            if(newDefs.isNotEmpty()) {
                // Apparently [arg] defeats [temp]
                // So check whether the status of the root changed.
                // If so, return a YES, after removing [newDefs] from [temp]
                // If not, only remove [newDefs] from [temp]

                if(arg in newDefs.map { it.argument }) {
                    argChangesTree = true
                }

                if(oldMarking != tree!!.marking) {
                    temp.children.removeAll(newDefs)

                    return ChangesStatusOfRootResult(true, true)
                } else {
                    temp.children.removeAll(newDefs)
                }
            }

        }

        return ChangesStatusOfRootResult(false, argChangesTree)
    }

    fun changesTree(arg: DelpArgument): Boolean =
            callGetDefeaters(this.tree!!, argumentsMoved, setOf(arg), criterion, false, letTreeUseBlackAndHunter).isNotEmpty()

    fun constructTree() {
        callGetDefeaters(tree!!, argumentsMoved, setOf(rootArgument!!), criterion, true, letTreeUseBlackAndHunter)
    }

    private fun construct(db: DefeasibleLogicProgram, root: DelpArgument, addToChild: Boolean): IBetterDiacTree {
        val tree = diacTreeGenerator(null, root)

        callGetDefeaters(tree, db, db.arguments, criterion, addToChild, letTreeUseBlackAndHunter)

        return tree
    }

    companion object {

        fun callGetDefeaters(treee: IBetterDiacTree, db: DefeasibleLogicProgram, arguments: Set<DelpArgument>, cmp: ComparisonCriterion, addAsChild: Boolean, letTreeUseBlackAndHunter: Boolean): Set<IBetterDiacTree> {
            val front = LinkedList<IBetterDiacTree>()

            val newDefs = HashSet(treee.getNewDefeaters(arguments, db, cmp, addAsChild, letTreeUseBlackAndHunter))

            front.add(treee)

            while (!front.isEmpty()) {
                val temp = front.remove()

                for (child in temp.children) {
                    front.offer(child)
                }

                val new = temp.getNewDefeaters(arguments, db, cmp, addAsChild, letTreeUseBlackAndHunter)

                newDefs.addAll(new)

                // Only when using the bh tree (when it is possible that adding a defeater creates a new argumentation
                // line) add the new defeaters to the front
                if(letTreeUseBlackAndHunter) {
                    front.addAll(new)
                }
            }

            return newDefs
        }
    }
}
