/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib

object Logger {
    var enabled: Boolean = true
    var sink: ILogger = ConsoleLogger()
    var logLevel: LogLevel = LogLevel.D

    private fun Log(agent: AgentId, level: LogLevel, message: String) {
        sink.Log(agent, level, message, enabled, logLevel)
    }

    fun stdin(text: Any) = sink.ToStdIn(text.toString())

    fun getStdin(): String = sink.getStdIn()

    fun d(agent: AgentId, message: String) {
        Log(agent, LogLevel.D, message)
    }

    fun i(agent: AgentId, message: String) {
        Log(agent, LogLevel.I, message)
    }

    fun w(agent: AgentId, message: String) {
        Log(agent, LogLevel.W, message)
    }

    fun e(agent: AgentId, message: String) {
        Log(agent, LogLevel.E, message)
    }
}
