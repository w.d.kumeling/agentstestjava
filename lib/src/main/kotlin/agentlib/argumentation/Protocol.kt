/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//
// Translated by CS2J (http://www.cs2j.com): 5-2-2018 14:37:45
//

package agentlib.argumentation

import net.sf.tweety.commons.BeliefBase
import java.util.*

/**
 * TODO: Figure out how to get rid of the nullable types here.
 */
abstract class Protocol<T : ILocution, TEnum : Enum<TEnum>, T2 : BeliefBase> protected constructor() : IProtocol<T, TEnum, T2> {
    private val templates: HashMap<String, MutableList<IDialogueTemplate<T, TEnum, T2>>> = HashMap()

    private val moveToTemplate: HashMap<Class<out IMove<T>>, IDialogueTemplate<T, TEnum, T2>> = HashMap()

    override fun getTemplates(dialogueType: String): List<IDialogueTemplate<T, TEnum, T2>> {
        return templates[dialogueType]!!
    }

    override fun addTemplate(template: IDialogueTemplate<T, TEnum, T2>, dialogueType: String, typeOfMove: Class<out IMove<T>>) {
        if (!templates.containsKey(dialogueType)) {
            templates[dialogueType] = ArrayList()
        }

        templates.get(dialogueType)!!.add(template)

        moveToTemplate[typeOfMove] = template

    }

    abstract override fun dialogueShouldEnd(history: IDialogueHistory<T>): Boolean

    override fun getCorrespondingTemplate(classOfMove: Class<out IMove<T>>): IDialogueTemplate<T, TEnum, T2>? {
        return moveToTemplate[classOfMove]
    }

}


