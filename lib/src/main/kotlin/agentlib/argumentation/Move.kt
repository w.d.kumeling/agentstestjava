/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib.argumentation

import agentlib.AgentId

import java.util.ArrayList
import java.util.Objects

abstract class Move<T : ILocution>(override val from: AgentId, override val addressedTo: AgentId, override val propositions: ArrayList<T>) : IMove<T> {
    abstract override val name: String

    constructor(from: AgentId, addressedTo: AgentId, proposition: T) : this(from, addressedTo, ArrayList<T>(listOf<T>(proposition))) {}

    abstract override fun clone(): Move<T>

    override fun toString(): String {
        return "$name $propositions"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val move = other as Move<*>?
        return addressedTo == move!!.addressedTo &&
                from == move.from &&
                propositions == move.propositions
    }

    override fun hashCode(): Int {
        return Objects.hash(addressedTo, from, propositions)
    }
}
