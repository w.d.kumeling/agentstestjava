/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib.argumentation

import net.sf.tweety.commons.BeliefBase
import java.util.function.Consumer

abstract class TurnTakingRule<T : ILocution, TEnum : Enum<TEnum>, T2 : BeliefBase> protected constructor(override val agents: MutableList<ArgumentationAgent<T, TEnum, T2>>) : ITurnTakingRule<T, TEnum, T2> {

    override val otherAgents: List<ArgumentationAgent<T, TEnum, T2>>
        get() = agents.filter { it.id != currentAgent.id }

    override lateinit var currentAgent: ArgumentationAgent<T, TEnum, T2>

    override lateinit var firstAgent: ArgumentationAgent<T, TEnum, T2>

    abstract override fun findNextAgent()

    override fun reset() {
        agents.forEach(Consumer<ArgumentationAgent<T, TEnum, T2>> { it.reset() })
    }

    override fun AddAgent(agent: ArgumentationAgent<T, TEnum, T2>) {
        agents.add(agent)
    }
}
