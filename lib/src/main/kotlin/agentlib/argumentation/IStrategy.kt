/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib.argumentation

import java.util.*

/**
 * A strategy is a function which, given a list of legal moves and possibly other arguments, selects one move.
 * For example, it could always return the first moves, or use whatever extra information is passed along.
 * @param <T> The type of the locutions used.
 * @param <T2> The type of the extra information passed along.
</T2></T> */
interface IStrategy<T : ILocution> {
    /**
     * Returns a move from the list of legal ones, or none at all.
     * @param moves The list of current legal moves.
     * @return A move if the agent chooses to move.
     */
    fun chooseMove(moves: Collection<IMove<T>>,
                   history: IDialogueHistory<T>,
                   otherAgent: ArgumentationAgent<T, *, *>,
                   extra: ExtraInformation): Optional<IMove<T>>

}


