/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib.argumentation.movehistory

import agentlib.AgentId
import agentlib.Logger
import agentlib.argumentation.IDialogueHistory
import agentlib.argumentation.ILocution
import agentlib.argumentation.IMove
import agentlib.argumentation.movecheckers.DefaultMoveChecker
import agentlib.argumentation.movecheckers.MoveEqualsChecker
import net.sf.tweety.commons.Formula

import java.text.MessageFormat
import java.util.*

open class MoveHistory<T : ILocution, T2 : Formula> : IDialogueHistory<T> {
    var moves = LinkedList<IMove<T>>()
        private set
    private val topicOfDialogue: Stack<HashSet<T2>> = Stack()
    private val typeOfDialogue: Stack<String> = Stack()

    private val id = AgentId("HIST")

    val currentTopic: HashSet<T2>
            get() = topicOfDialogue.peek()

    var moveEqualsChecker: MoveEqualsChecker<T> = DefaultMoveChecker()

    val parentTopic: Optional<HashSet<T2>>
            get() = when {
                topicOfDialogue.size < 2 -> Optional.empty()
                else -> Optional.of(topicOfDialogue[topicOfDialogue.size - 2])
            }

    val lastMove: IMove<T>
            get() = moves.last

    val isTopicEmpty: Boolean = topicOfDialogue.size == 0

    override val length
            get() = moves.size

    override val currentDialogueType
            get() = typeOfDialogue.peek()

    protected fun getFirstTopicOfDialogue(dialoguetype: String): Optional<HashSet<T2>> {
        val index = typeOfDialogue.indexOf(dialoguetype)

        return if (index < 0) {
            Optional.empty()
        } else Optional.of(topicOfDialogue[index])

    }

    override fun openSubdialogue(type: String, copyTopic: Boolean) {
        Logger.d(id, MessageFormat.format("Opening new subdialogue: {0}", type))
        typeOfDialogue.push(type)

        if (copyTopic) {
            topicOfDialogue.push(topicOfDialogue.peek())
        } else {
            topicOfDialogue.push(HashSet(4))
        }
    }

    override fun closeCurrentDialogue() {
        Logger.d(id, MessageFormat.format("Closing subdialogue: {0}", currentTopic))
        typeOfDialogue.pop()
        topicOfDialogue.pop()
    }

    override fun reset() {
        // Let's remove things untill the kick-start dialogue remanins
        while (topicOfDialogue.size > 1) {
            topicOfDialogue.pop()
            typeOfDialogue.pop()
        }

        // Furthermore let's empty the move list
        moves = LinkedList()
    }

    override fun addMove(move: IMove<T>) {
        synchronized(moves) {
            moves.addLast(move)
        }
    }

    fun addToCurrentTopic(item: T2) {
        topicOfDialogue.peek().add(item)
    }

    fun clearCurrentTopic() {
        topicOfDialogue.peek().clear()
    }

    fun removeFromCurrentTopic(item: T2) {
        topicOfDialogue.peek().remove(item)
    }

    operator fun contains(move: IMove<T>): Boolean {
        return moves.any { m -> moveEqualsChecker.moveEquals(m, move) }
    }

    operator fun get(i: Int): IMove<T> {
        return moves[i]
    }
}
