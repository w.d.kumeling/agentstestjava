/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//
// Translated by CS2J (http://www.cs2j.com): 5-2-2018 14:37:44
//

package agentlib.argumentation

import agentlib.AgentId
import net.sf.tweety.commons.BeliefBase

/**
 * An agent who reacts to instructions from the referee
 * The type of proposition the IMove contains.
 */
abstract class ArgumentationAgent<T : ILocution, TEnum : Enum<TEnum>, T2 : BeliefBase>(
        val roleOfAgent: TEnum,
        val strategy: IStrategy<T>,
        private val acceptance: AcceptanceAttitude<T2, T>,
        private val assertion: AssertionAttitude<T2, T>) {

    var id: AgentId
        private set

    init {
        this.id = AgentId(roleOfAgent.toString())
    }

    lateinit var beliefBase: T2
    lateinit var commitmentStore: T2
        protected set
    /**
     * The initial question the agent may have.
     */
    lateinit var initialQuestion: T

    /**
     * If set to true, tell the agent to ignore its attitudes and always return what the templates want to hear.
     */
    var isIgnoringAttitudes: Boolean = false


    /**
     * Returns the extra information that should be passed to its own strategy.
     * @return The extra information.
     */
    open val extraInformationForStrategy: ExtraInformation = ExtraInformation.EMPTY

    /***
     * Should reset the agent to the situation before the dialogue.
     * This usually means without a commitment store.
     */
    abstract fun reset()

    fun isAssertable(opponentsCommitment: T2, proposition: T, trueToSucceed: Boolean): Boolean {
        return if (isIgnoringAttitudes) {
            trueToSucceed
        } else {
            assertion.propositionIsAssertable(beliefBase, commitmentStore, opponentsCommitment, proposition)
        }
    }

    fun isAcceptable(opponentsCommitment: T2, proposition: T, trueToSucceed: Boolean): Boolean {
        return if (isIgnoringAttitudes) {
            trueToSucceed
        } else {
            acceptance.propositionIsAcceptable(beliefBase, commitmentStore, opponentsCommitment, proposition)
        }
    }

    override fun toString(): String {
        return hashCode().toString()
    }
}


