/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib.argumentation

import net.sf.tweety.commons.BeliefBase

interface ITurnTakingRule<T : ILocution, TEnum : Enum<TEnum>, T2 : BeliefBase> {

    /**
     * Fill [currentAgent] with the next agent to take the turn
     */
    fun findNextAgent()

    /**
     * The agent whose turn it is currently.
     */
    var currentAgent: ArgumentationAgent<T, TEnum, T2>

    /**
     * All agents except for the [currentAgent].
     */
    val otherAgents: List<ArgumentationAgent<T, TEnum, T2>>

    /**
     * Get the list with all agents.
     * @return A list containing all agents active in the current discussion.
     */
    val agents: List<ArgumentationAgent<T, TEnum, T2>>

    /**
     * Get which agent should go first.
     *
     * @return The first agent to have a turn.
     */
    /***
     * Set which agent should get the first turn. A subsequent call to [getNextAgent][.getNextAgent]
     * will give the next agent whose turn it is.
     * @param agent The agent which should get the first turn.
     */
    var firstAgent: ArgumentationAgent<T, TEnum, T2>

    /**
     * Add an agent to the turn taking
     *
     * @param agent The agent to add to the turn considering.
     */
    fun AddAgent(agent: ArgumentationAgent<T, TEnum, T2>)

    /**
     * Set the turn taking rule to a situation BEFORE the dialogue started.
     */
    fun reset()
}
