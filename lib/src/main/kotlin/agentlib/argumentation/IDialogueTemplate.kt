/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib.argumentation

import net.sf.tweety.commons.BeliefBase

/**
 * An interface defining what a template is, as described in: 'B. Testerink and F. J. Bex, "Specifications for Peer-to-Peer Argumentation Dialogues," 2016.'
 */
interface IDialogueTemplate<T : ILocution, TEnum : Enum<TEnum>, T2 : BeliefBase> {
    /**
     * Check whether the template can be applied. (i.e. are the necessary presumptions satisfied?)
     *
     * @param history
     * @param hisTurn
     * @param addressTo
     * @return Whether, given the dialogue, this template is applicable.
     */
    fun isApplicable(history: IDialogueHistory<T>, hisTurn: ArgumentationAgent<T, TEnum, T2>, addressTo: ArgumentationAgent<T, TEnum, T2>, roleOfTurnTaker: TEnum): Boolean

    /**
     * Returns the move to be sent.
     */
    fun getMatchingMoves(history: IDialogueHistory<T>, hisTurn: ArgumentationAgent<T, TEnum, T2>, addressTo: ArgumentationAgent<T, TEnum, T2>, roleOfTurnTaker: TEnum): List<IMove<T>>

    /**
     * Apply the move, given the current state
     */
    fun apply(history: IDialogueHistory<T>, hisTurn: ArgumentationAgent<T, TEnum, T2>, moveToApply: IMove<T>)
}


