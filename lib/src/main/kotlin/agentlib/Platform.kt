/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib

import agentlib.argumentation.*
import net.sf.tweety.commons.BeliefBase
import java.text.MessageFormat
import java.util.*

class Platform<T : Enum<T>, T2 : ILocution, T3 : BeliefBase>(private val protocol: Protocol<T2, T, T3>, val history: IDialogueHistory<T2>, private val turnTakingRule: ITurnTakingRule<T2, T, T3>) {

    private val listeners = LinkedList<(Platform<T, T2, T3>) -> Unit>()

    fun addOnstopListener(ls: (Platform<T, T2, T3>) -> Unit): Platform<T, T2, T3> {
        listeners.add(ls)

        return this
    }

    fun removeOnstopListener(ls: (Platform<T, T2, T3>) -> Unit): Platform<T, T2, T3> {
        listeners.remove(ls)

        return this
    }

    /**
     * Reset everything to the point before any dialogue was taking place.
     */
    fun reset(): Platform<T, T2, T3> {
        history.reset()
        turnTakingRule.reset()

        isDialogueStopped = false

        return this
    }

    fun addAgent(agent: ArgumentationAgent<T2, T, T3>) {
        turnTakingRule.AddAgent(agent)
    }

    fun getAgent(agentid: AgentId): ArgumentationAgent<T2, T, T3> {
        return turnTakingRule.agents
                .first {  ag -> ag.id == agentid }
    }

    fun run() {
        while (!isDialogueStopped) {
            step()
            // Rinse and repeat
        }

        // When stopping, notify listeners.
        for (list in listeners) {
            list(this)
        }
    }

    var isDialogueStopped: Boolean = false

    /**
     * @return Whether the dialogue should end right now.
     */
    @JvmOverloads
    fun step(n:Int = 1) {
        assert(!isDialogueStopped)

        for (i in 1..n) {
            if (isDialogueStopped) {
                return
            }

            // Find the turn taker
            turnTakingRule.findNextAgent()

            Logger.i(PlatformId, MessageFormat.format("{0} has got the turn.", turnTakingRule.currentAgent))

            // Assume that there are only two agents
            assert(turnTakingRule.otherAgents.size == 1)

            // Get the available moves
            val moves = getLegalMoves()

            Logger.i(PlatformId, MessageFormat.format("Found {0} legal moves.", moves.size))
            Logger.d(PlatformId, MessageFormat.format("Found moves {0} .", moves))

            // Choose a move
            val chosenMove = chooseMove(moves)

            Logger.i(PlatformId, MessageFormat.format("Move {0} was chosen.", chosenMove))

            // End dialogue iff we chose no move, or if there were none.
            endDialogueIfNoMoveIsChosen(moves, chosenMove)

            // Add the move to the history, only if the dialogue has not ended.
            if (!isDialogueStopped) {
                addAndApplyMove(chosenMove)
            }
        }
    }

    fun endDialogueIfNoMoveIsChosen(moves: ArrayList<IMove<T2>>, chosenMove: Optional<IMove<T2>>){
        if (moves.size == 0 || !chosenMove.isPresent) {
            // No move chosen, assume the dialogue ends here.
            isDialogueStopped = true
        }
    }

    /**
     * Add and apply the move to the dialogue. Also optionally end the dialogue
     */
    fun addAndApplyMove(chosenMove: Optional<IMove<T2>>) {
        history.addMove(chosenMove.get())

        // Apply the move
        val typeOfMove = chosenMove.get().javaClass
        protocol.getCorrespondingTemplate(typeOfMove)!!.apply(history, turnTakingRule.currentAgent, chosenMove.get())


        if(protocol.dialogueShouldEnd(history)) {
            isDialogueStopped = true
        }
    }

    /**
     * Choose a move, assuming there are only two participants.
     */
    fun chooseMove(legalMoves: ArrayList<IMove<T2>>) =
            turnTakingRule.currentAgent.strategy.chooseMove(
                    legalMoves,
                    history,
                    turnTakingRule.otherAgents[0],
                    turnTakingRule.currentAgent.extraInformationForStrategy)

    fun getLegalMoves(): ArrayList<IMove<T2>> {
        val moves = ArrayList<IMove<T2>>(7)

        val theTurnTaker = turnTakingRule.currentAgent

        val others = turnTakingRule.otherAgents

        val currentType = history.currentDialogueType

        for (template in protocol.getTemplates(currentType)) {
            for (other in others) {
                if (template.isApplicable(history, theTurnTaker, other, theTurnTaker.roleOfAgent)) {
                    moves.addAll(template.getMatchingMoves(history, theTurnTaker, other, theTurnTaker.roleOfAgent))
                }
            }
        }
        return moves
    }

    companion object {
        var PlatformId = AgentId("PLAT")
    }
}
