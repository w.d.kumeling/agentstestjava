/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package randomaspic;

import agentlib.AspicUtils;
import net.sf.tweety.arg.aspic.AspicArgumentationTheory;
import net.sf.tweety.arg.aspic.ruleformulagenerator.PlFormulaGenerator;
import net.sf.tweety.arg.aspic.syntax.DefeasibleInferenceRule;
import net.sf.tweety.arg.aspic.syntax.InferenceRule;
import net.sf.tweety.arg.aspic.syntax.StrictInferenceRule;
import net.sf.tweety.logics.pl.syntax.Negation;
import net.sf.tweety.logics.pl.syntax.Proposition;
import net.sf.tweety.logics.pl.syntax.PropositionalFormula;
import net.sf.tweety.logics.pl.syntax.PropositionalSignature;

import java.util.*;

public class RandomAspicGenerator {
    private Random rand = new Random();
    private List<Proposition> signature;


    public int maxRules = 10;
    public int maxPredicates = 3;
    public double chanceOfStrictRule = 0.5;
    public double chanceOfNegatedProposition = 0.2;

    public RandomAspicGenerator(PropositionalSignature signature) {
        this.signature = new ArrayList<>(signature);
    }

    public PropositionalFormula getRandomLiteral(boolean zeroPredicates) {
        return getRandomLiteral(zeroPredicates, Collections.EMPTY_LIST);
    }

    public PropositionalFormula getRandomLiteral(boolean zeroPredicates, Collection<? extends PropositionalFormula> ignore) {
        if(ignore.size() > signature.size()) {
            throw new IllegalArgumentException("ignore too big.");
        }

        Proposition proposition;

        do {
            proposition = signature.get(rand.nextInt(signature.size()));
        } while (ignore.contains(proposition));

        return (zeroPredicates && rand.nextDouble() < chanceOfNegatedProposition) ? new Negation(proposition) : proposition;
    }

    public StrictInferenceRule<PropositionalFormula> getRandomStrictRule() {
        int numPredicates = rand.nextInt(maxPredicates);
        StrictInferenceRule<PropositionalFormula>  rule = new StrictInferenceRule<>();

        for (int i = 0; i < numPredicates; i++) {
            rule.addPremise(getRandomLiteral(true));
        }

        rule.setConclusion(getRandomLiteral(numPredicates > 0, rule.getPremise()));
        return rule;
    }

    public DefeasibleInferenceRule<PropositionalFormula> getRandomDefeasibleRule() {
        int numPredicates = rand.nextInt(maxPredicates);
        DefeasibleInferenceRule<PropositionalFormula>  rule = new DefeasibleInferenceRule<>();


        for (int i = 0; i < numPredicates; i++) {
            rule.addPremise(getRandomLiteral(true));
        }

        rule.setConclusion(getRandomLiteral(numPredicates > 0, rule.getPremise()));
        return rule;
    }

    public InferenceRule<PropositionalFormula> getRandomRule() {
        if (rand.nextDouble() < chanceOfStrictRule) {
            return getRandomStrictRule();
        } else {
            return getRandomDefeasibleRule();
        }
    }

    public AspicArgumentationTheory<PropositionalFormula> getRandomArgumentationTheory() {
        int numRules = rand.nextInt(maxRules);
        AspicArgumentationTheory<PropositionalFormula> theory = new AspicArgumentationTheory<>(new PlFormulaGenerator());

        for (int i = 0; i < numRules + 1; i++) {
            theory.addRule(getRandomRule());
        }

        return theory;
    }

    public AspicArgumentationTheory<PropositionalFormula> makeReasonable(AspicArgumentationTheory<PropositionalFormula> sucks) {
        // Ok, so our randomly generated theory sucks.
        Collection<InferenceRule<PropositionalFormula>> rules = sucks.getRules();

        // First collect all literals
        HashSet<PropositionalFormula> literals = new HashSet<>();

        for (InferenceRule<PropositionalFormula> rule : rules) {
            literals.addAll(AspicUtils.getLiteralsOf(rule));
        }

        // Then collect all axioms and ordinary premises
        List<InferenceRule<PropositionalFormula>> axiomsAndPremises = new ArrayList<>(10);

        for (InferenceRule<PropositionalFormula> rule : rules) {
            if(rule.getPremise().size() == 0) {
                axiomsAndPremises.add(rule);
            }
        }
        rules.removeAll(axiomsAndPremises);

        List<InferenceRule<PropositionalFormula>> toRemove = new ArrayList<>();

        // Remove rules with the same thing before and after the arrow sign
        for (InferenceRule<PropositionalFormula> rule : rules) {
            PropositionalFormula conclusion;

            if(rule.getConclusion() instanceof Negation) {
                conclusion = ((Negation) rule.getConclusion()).getFormula();
            } else {
                conclusion = rule.getConclusion();
            }

            if(rule.getPremise().contains(conclusion) || rule.getPremise().contains(new Negation(conclusion))) {
                toRemove.add(rule);
            }
        }

        rules.removeAll(toRemove);
        toRemove.clear();

        // Remove double axioms and ordinary premises
        // If a double is found, select the second one.
        for (InferenceRule<PropositionalFormula> premise : axiomsAndPremises) {
            if(axiomsAndPremises.stream()
                    .filter(r2 -> !premise.equals(r2) && !toRemove.contains(r2))
                    .map(InferenceRule::getConclusion)
                    .anyMatch(r2 -> r2.equals(premise.getConclusion()))) {
                toRemove.add(premise);
            }
        }

        axiomsAndPremises.removeAll(toRemove);
        toRemove.clear();

        AspicArgumentationTheory<PropositionalFormula> newTheory = new AspicArgumentationTheory<>(new PlFormulaGenerator());

        for (InferenceRule<PropositionalFormula> premise : axiomsAndPremises) {
            newTheory.addRule(premise);
        }

        for (InferenceRule<PropositionalFormula> rule : rules) {
            newTheory.addRule(rule);
        }

        return newTheory;
    }
}
