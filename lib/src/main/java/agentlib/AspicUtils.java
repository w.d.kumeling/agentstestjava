/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib;

import net.sf.tweety.arg.aspic.AspicArgumentationTheory;
import net.sf.tweety.arg.aspic.AspicReasoner;
import net.sf.tweety.arg.aspic.ruleformulagenerator.PlFormulaGenerator;
import net.sf.tweety.arg.aspic.syntax.AspicArgument;
import net.sf.tweety.arg.aspic.syntax.DefeasibleInferenceRule;
import net.sf.tweety.arg.aspic.syntax.InferenceRule;
import net.sf.tweety.arg.dung.semantics.Semantics;
import net.sf.tweety.commons.Reasoner;
import net.sf.tweety.logics.pl.syntax.Proposition;
import net.sf.tweety.logics.pl.syntax.PropositionalFormula;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AspicUtils {
    private AspicUtils(){}

    public static List<AspicArgument<PropositionalFormula>> getArgumentsWithConclusion(AspicArgumentationTheory<PropositionalFormula> theory, Proposition conclusion, boolean includeConclusion) {
        DefeasibleInferenceRule<PropositionalFormula> d1 = new DefeasibleInferenceRule<>();
        d1.setConclusion(conclusion);
        AspicArgument<PropositionalFormula> arg2 = new AspicArgument<>(d1);

        List<AspicArgument<PropositionalFormula>> lis = theory.getArguments()
                .stream()
                .filter(arg -> arg.getConclusion().equals(conclusion))
                .collect(Collectors.toList());

        if(!includeConclusion) {
            lis.remove(arg2);
        }

        return lis;

    }

    public static Reasoner getReasoner(AspicArgumentationTheory<PropositionalFormula> mineBelief, AspicArgumentationTheory<PropositionalFormula> mineCommitment, AspicArgumentationTheory<PropositionalFormula> hisCommitment) {
        AspicArgumentationTheory<PropositionalFormula> joined = join(mineBelief, mineCommitment);

        if(hisCommitment != null) {
            joined = join(joined, hisCommitment);
        }

        return new AspicReasoner(joined, Semantics.GROUNDED_SEMANTICS, Semantics.CREDULOUS_INFERENCE);
    }

    public static List<AspicArgument<PropositionalFormula>> getSupportOf(AspicArgumentationTheory<PropositionalFormula> theory, PropositionalFormula prop) {
        return theory.getArguments()
                .stream()
                .filter(arg -> arg.getConclusion().equals(prop))
                .flatMap(arg -> arg.getDirectSubs().stream())
                .collect(Collectors.toList());
    }

    public static List<PropositionalFormula> getLiteralsOf(AspicArgument<PropositionalFormula> argument) {
        return argument.getOrdinaryPremises()
                .stream()
                .map(argument1 -> argument1.getTopRule().getConclusion())
                .collect(Collectors.toList());
    }

    public static List<PropositionalFormula> getLiteralsOf(InferenceRule<PropositionalFormula> argument) {
        List<PropositionalFormula> prop = new ArrayList<>(argument.getPremise());
        prop.add(argument.getConclusion());

        return prop;
    }


    @SafeVarargs
    public static AspicArgumentationTheory<PropositionalFormula> join(AspicArgumentationTheory<PropositionalFormula>... left) {
        return Arrays
                .stream(left)
                .reduce(new AspicArgumentationTheory<>(new PlFormulaGenerator()), AspicUtils::join);
    }

    public static AspicArgumentationTheory<PropositionalFormula> join(AspicArgumentationTheory<PropositionalFormula> left, AspicArgumentationTheory<PropositionalFormula> right) {
        AspicArgumentationTheory<PropositionalFormula> theory2 = new AspicArgumentationTheory<>(new PlFormulaGenerator());

        left.getRules().forEach(theory2::addRule);
        right.getRules().forEach(theory2::addRule);

        return theory2;
    }

    public static boolean equalsForRules(InferenceRule<PropositionalFormula> one, InferenceRule<PropositionalFormula> other) {
        if(!one.getClass().equals(other.getClass())) {
            return false;
        }

        if(!one.getConclusion().equals(other.getConclusion())) {
            return false;
        }

        if(!one.getPremise().equals(other.getPremise())) {
            return false;
        }

        return true;
    }
}
